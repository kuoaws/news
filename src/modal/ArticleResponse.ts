export interface IArticleResponse {
    status: string;
    totalResults: number;
    articles: Array<IArticle>
}

export interface IArticle {
    author: string;
    content: string;
    description: string;
    publishedAt: string;
    title: string;
    url: string;
    urlToImage: string;
}