import React, { useState } from 'react'
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';

const SearchBar = () => {
    const [input, setInput] = useState('')

    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setInput(e.target.value)
    }

    return (
        <div>
            <TextField
                value={input}
                onChange={handleInputChange}
            />

        </div>

    )
}

export default SearchBar