import React from 'react'
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';

import { IArticle } from '../modal/ArticleResponse'

export const Article = (props: IArticle) => {

    const { author, content, description, publishedAt, title, url, urlToImage } = props;

    return (
        <div>
            <Card elevation={3}>
                <CardHeader
                    title={title}
                />
                <CardContent>
                    <Typography variant='body1' color='textSecondary'>{description}</Typography>
                </CardContent>
            </Card>
        </div>
    )
}
