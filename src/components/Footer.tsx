import React from 'react'
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';

const Footer = () => {
    return (
        <div>
            <Typography variant="body2" color="text.secondary" align="center" sx={{ mt: 8, mb: 4 }}>
                <Link color="inherit" href="#">
                    cookie
                </Link>{' '}

                <Link color="inherit" href="#">
                    feedback
                </Link>{' '}

                <Link color="inherit" href="#">
                    AD
                </Link>{' '}

                {new Date().getFullYear()}
                {'.'}
            </Typography>
        </div>
    )
}

export default Footer