import React, { PropsWithChildren, useContext } from 'react'
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';

import Search from '@mui/material/AppBar';
import SearchIconWrapper from '@mui/material/AppBar';
import InputBase from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';


import { makeStyles } from 'tss-react/mui';
import { useNavigate, useLocation } from 'react-router-dom';

import '../styles/Layout.css'
import { Button, ListItemButton } from '@mui/material';

import { AppContext } from '../store/app'

interface IMenu {
    text: string;
    icon: React.ReactNode;
    path: string;
}

const DRAWER_WIDTH = 230;

const useStyles = makeStyles()(theme => {
    return {
        loginBtn: {
            alignSelf: 'flex-end'
        },
        active: {
            background: '#f4f4f4'
        }
    }
})

const Layout: React.FC<PropsWithChildren> = ({ children }) => {

    const { classes } = useStyles();

    const navigate = useNavigate();
    const location = useLocation();

    const ctx = useContext(AppContext);

    const menus: Array<IMenu> = [
        {
            text: 'My Notes',
            icon: <ArrowForwardIosIcon color='secondary' />,
            path: '/'
        },
        {
            text: 'Create Note',
            icon: <ArrowForwardIosIcon color='secondary' />,
            path: '/create'
        },
    ];

    const handleNavPage = (menu: IMenu) => {
        ctx.changePageTitle(menu.text);
        navigate(menu.path);
    }

    return (
        <div className='layout'>
            <div className='header'>
                <AppBar position='static'>
                    <Toolbar>
                        <IconButton
                            edge="start"
                            size='large'
                            color="inherit"
                            aria-label="menu"
                            sx={{ mr: 1 }}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant='h6' sx={{ flexGrow: 1 }}>News</Typography>

                        <Button color="inherit">Login</Button>

                    </Toolbar>
                </AppBar>

            </div>
            <div className='container'>
                <div className="nav">
                    <List>
                        {
                            menus.map(menu => {
                                return (
                                    <ListItem
                                        key={Math.random()}
                                        className={location.pathname == menu.path ? classes.active : undefined}
                                    >
                                        <ListItemButton onClick={handleNavPage.bind(this, menu)}>
                                            <ListItemIcon>
                                                <ArrowForwardIosIcon />
                                            </ListItemIcon>
                                            <ListItemText primary={menu.text} />
                                        </ListItemButton>
                                    </ListItem>
                                )
                            })
                        }
                    </List>

                </div>
                <div className="content">{children}</div>
            </div>
        </div>
    )
}

export default Layout
