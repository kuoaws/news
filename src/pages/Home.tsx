import React, { useEffect, useState, useContext } from 'react'
import axios from 'axios'
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import SearchBar from '../components/SearchBar'
import Navigation from '../components/Navigation';
import { Article } from '../components/Article'
import { IArticleResponse, IArticle } from '../modal/ArticleResponse'
import Footer from '../components/Footer';

import { AppContext } from '../store/app'

const Home = () => {
    const [articles, setArticles] = useState<Array<IArticle>>([]);

    const api = "http://localhost:3000/news"

    const ctx = useContext(AppContext);

    useEffect(() => {
        const getData = async () => {
            const { data } = await axios.get<IArticleResponse>(api);
            setArticles(data.articles)
        }

        getData();
    }, [])

    return (
        <Container>
            <Typography
                variant='h6'
                component='h2'
                color='textSecondary'
                gutterBottom
            >
                {ctx.pageTitle}
            </Typography>
            <Grid container spacing={3}>
                {
                    articles.map((article) => {
                        return (
                            <Grid key={Math.random()} item xs={12} md={4}>
                                <Article {...article} />
                            </Grid>
                        )
                    })
                }
            </Grid>
        </Container>

    )
}

export default Home