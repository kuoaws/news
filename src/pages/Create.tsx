import React, { useContext } from 'react'
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import AcUnitIcon from '@mui/icons-material/AcUnit';
import TextField from '@mui/material/TextField';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import { makeStyles } from 'tss-react/mui';

import { AppContext } from '../store/app';

const useStyles = makeStyles()(theme => {
    return {
        field: {
            marginTop: 20,
            marginBottom: 20,
            display: 'block'
        }
    }
})

const Create = () => {

    const { classes } = useStyles();

    const ctx = useContext(AppContext);

    return (
        <Container>
            <Typography
                variant='h6'
                component='h2'
                color='textSecondary'
                gutterBottom
            >
                {ctx.pageTitle}
            </Typography>

            <form noValidate autoComplete='off'>
                <TextField
                    className={classes.field}
                    label='Note Title'
                    variant='outlined'
                    fullWidth
                    required
                />

                <TextField
                    className={classes.field}
                    label='Details'
                    variant='outlined'
                    multiline
                    rows={3}
                    fullWidth
                    required
                />

                <FormControl className={classes.field}>
                    <FormLabel>Note Category</FormLabel>
                    <RadioGroup value='work' onChange={() => { }}>
                        <FormControlLabel label="Money" value="money" control={<Radio />} />
                        <FormControlLabel label="Todos" value="todos" control={<Radio />} />
                        <FormControlLabel label="Reminders" value="reminders" control={<Radio />} />
                        <FormControlLabel label="Work" value="work" control={<Radio />} />
                    </RadioGroup>
                </FormControl>

                <Button
                    type='submit'
                    color='primary'
                    variant='contained'
                    endIcon={<AcUnitIcon />}
                >
                    Submit
                </Button>

            </form>


        </Container>
    )
}

export default Create