import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Layout from './components/Layout'
import Home from './pages/Home'
import Create from './pages/Create'
import { AppContextProvider } from './store/app'


const App = () => {
    return (
        <BrowserRouter>
            <AppContextProvider>
                <Layout>
                    <Routes>
                        <Route index element={<Home />} />
                        <Route path='create' element={<Create />} />
                    </Routes>
                </Layout>
            </AppContextProvider>
        </BrowserRouter>
    )
}

export default App