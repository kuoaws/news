import React, { PropsWithChildren, createContext, useState } from 'react'

export interface IArticleContext {
    title: string;
    changeTitle: (title: string) => void;
}

export const ArticleContext = createContext<IArticleContext | null>(null);

export const ArticleContextProvider: React.FC<PropsWithChildren> = ({ children }) => {
    const [title, setTitle] = useState('');

    const changeTitle = (title: string) => {
        setTitle(title)
    }

    const value: IArticleContext = {
        title,
        changeTitle: changeTitle
    }

    return (
        <ArticleContext.Provider value={value}>{children}</ArticleContext.Provider>
    )
}



