import React, { PropsWithChildren, createContext, useState } from 'react'

export interface IAppContext {
    pageTitle: string;
    changePageTitle: (pageTitle: string) => void;
}

const initAppContext: IAppContext = {
    pageTitle: '',
    changePageTitle: (pageTitle: string) => { }
}

export const AppContext = createContext<IAppContext>(initAppContext);

export const AppContextProvider: React.FC<PropsWithChildren> = ({ children }) => {
    const [pageTitle, setPageTitle] = useState('My Notes');

    const changePageTitle = (pageTitle: string) => {
        setPageTitle(pageTitle)
    }

    const value: IAppContext = {
        pageTitle,
        changePageTitle
    }

    return (
        <AppContext.Provider value={value} >{children}</AppContext.Provider>
    )

}